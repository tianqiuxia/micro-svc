package com.test.auth.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.test.auth.entity.ComResourcesEntity;



@Mapper
public interface ComResourcesMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(ComResourcesEntity record);

    int insertSelective(ComResourcesEntity record);

    ComResourcesEntity selectByPrimaryKey(Integer id);

    List<ComResourcesEntity> selectByCondition(Map<String, Object> map);

    int updateByPrimaryKeySelective(ComResourcesEntity record);

    int updateByPrimaryKey(ComResourcesEntity record);
    
    int batchInsert(@Param("list")List<ComResourcesEntity> list);
    int batchUpdate(@Param("list")List<ComResourcesEntity> list);
    int batchDelete(@Param("list")List<ComResourcesEntity> list);
    
    
}